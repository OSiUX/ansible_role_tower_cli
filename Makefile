ROLE=gcoop-libre.tower_cli

DEBUG ?= -v
SUDO ?=

role:
	mkdir -p tests/roles
	rm -f tests/roles/$(ROLE)
	cd tests/roles && ln -s ../../. $(ROLE)
	cd ..
	ansible-playbook $(DEBUG) -i tests/inventory $(SUDO) tests/test.yml

lint:
	mkdir -p tests/roles
	rm -f tests/roles/$(ROLE)
	cd tests/roles && ln -s ../../. $(ROLE)
	cd ..
	ansible-lint $(DEBUG) tests/test.yml

organization-list:
	tower-cli organization list

user-list:
	tower-cli user list

inventory-list:
	tower-cli inventory list

host-list:
	tower-cli host list

credential-list:
	tower-cli credential list

project-list:
	tower-cli project list

job-template-list:
	tower-cli job_template list

job-list:
	tower-cli job list

list: organization-list user-list inventory-list host-list credential-list project-list job-template-list job-list

backup:
	tower-cli receive --all >backup-$$(date +\%F).json

plugins/lookup/pass/lookup_plugins/pass.py:
	mkdir -p plugins/lookup
	git clone https://github.com/gcoop-libre/ansible-lookup-plugin-pass.git plugins/lookup/pass

dependencies: plugins/lookup/pass/lookup_plugins/pass.py
