tower_cli
=========

An Ansible Role that configure Organization, Inventory, Group, Host,
Credential, Project and Job Template in AWX or Ansible Tower.

Requirements
------------

You need a ``~/.tower_cli.cfg`` of instance of AWX or Ansible Tower.

```ini

host=http://127.0.0.1:80
password=xxxxxxxxxx
username=admin
verify_ssl=false

```

Role Variables
--------------

Available variables are listed below, along with default values (see
`defaults/main.yml`):

```yaml

awx_config_file: '~/.tower_cli.cfg'
awx_waiting_backend: 15

awx_default_state: present
awx_default_verbosity: verbose

awx_default_organization: test_organization

awx_default_scm_branch: master
awx_default_scm_clean: no
awx_default_scm_credential: test_scm_credential
awx_default_scm_delete_on_update: no
awx_default_scm_server: test_scm_server
awx_default_scm_type: git
awx_default_scm_update_on_launch: no

awx_default_credential: test_credential
awx_default_ssh_key_data: ~/.ssh/id_rsa
awx_default_ssh_key_unlock: []
awx_default_inventory: test_inventory
awx_default_ansible_host: localhost
awx_default_ansible_user: ansible
awx_default_job_type: run
awx_default_playbook: main.yml
awx_default_project: test_project

awx_default_ask_credential: yes
awx_default_ask_extra_vars: no
awx_default_ask_inventory: no
awx_default_ask_job_type: no
awx_default_ask_tags: no
awx_default_become_enabled: no

awx_job_templates:
- name: test_job_template
  inventory: "{{ awx_default_inventory }}"
  project: "{{ awx_default_project }}"

awx_organizations:
- name: test_organization
  description: 'Test Organization'

awx_projects:
- name: test_project
  scm_url: "git@{{ awx_default_scm_server }}:{{ awx_default_project }}"

awx_credentials:
- name: "{{ awx_default_credential }}"
  ssh_key_data: "{{ awx_default_ssh_key_data }}"
  ssh_key_unlock: []
  kind: ssh
  organization: "{{ awx_default_organization }}"
  username: git
  state: present
- name: "{{ awx_default_scm_credential }}"
  ssh_key_data: "{{ awx_default_ssh_key_data }}"
  ssh_key_unlock: []
  kind: scm
  organization: "{{ awx_default_organization }}"
  username: git
  state: present

awx_inventories:
  - name: "{{ awx_default_inventory }}"

test_inventory:
  - name: test_inventory
    ansible_user: "{{ awx_default_ansible_user }}"
    ansible_host: "{{ awx_default_ansible_host }}"
    inventory: test_inventory

awx_hosts:
  - name: test_host
    ansible_user: "{{ awx_default_ansible_user }}"
    ansible_host: "{{ awx_default_ansible_host }}"
    inventory: test_inventory

awx_groups: []

```

Dependencies
------------

None.

Example Playbook
----------------

```yaml

- name: Configure AWX
  hosts: localhost
  connection: local
  become: no
  gather_facts: no

  roles:
    - gcoop-libre.tower_cli

```

License
-------

GNU General Public License, GPLv3

Author Information
------------------

This role was created in 2019 by
 [Osiris Alejandro Gomez](https://osiux.com/),
 worker cooperative of
 [gcoop Cooperativa de Software Libre](http://www.gcoop.coop/).
